using UnityEngine.UI;
using UnityEngine;
using System;

namespace VehiclePhysics.UI
{
    public class VPExtraVisuals : MonoBehaviour
    {
        private VehicleBase vehicle;
        enum TurnState { left, none, right}
        //public VehicleBase vehicle;
        [Header("Turn Signals")]
        public KeyCode[] turnSignalKeys;
        public TurnSignal leftSignal;
        public TurnSignal rightSignal;
        public AudioSource turnSignalSound;
        enum TurnSignalDisplayMode { Flashing, Solid }
        [SerializeField] private TurnSignalDisplayMode displayMode;
        private TurnState currentTurn = TurnState.none;
        private bool turnChanged = true;
        private bool turnDirectionSwitched = false;
        public uint blinkDuration;
        private uint timeElapsed;

        [Serializable]
        public class TurnSignal
        {
            [HideInInspector]
            public bool on;
            public Image inCarIndicator;
            public GameObject[] lights;
        }
        
        [Header("Dashboard")]
        public Text gearLabel;
        [Header("Car Music Player")]
        public MusicPlaylist carMusicPlaylist;
        public KeyCode[] playPauseKeys;
        public KeyCode[] nextSongKeys;
        public KeyCode[] previousSongKeys;

        [Header("Miscellaneous")]
        public GameObject interiorLight;
        public KeyCode[] interiorLightKeys;
        public Animator windshieldWiperAnimation;
        public KeyCode[] windshieldWiperKeys;

        void OnEnable ()
		{
            if (vehicle == null)
                vehicle = GetComponentInParent<VehicleBase>();
		}
        void Start()
        {
            if (carMusicPlaylist != null)
                carMusicPlaylist.Play();
        }
        // Update is called once per frame
        void Update()
        {
            foreach (KeyCode k in turnSignalKeys)
            {
                if (Input.GetKeyDown(k))
                {
                    SwitchTurnSignal();
                    break;
                }
            }
            // blink turn signal lights depending on the turn signal state
            if (displayMode == TurnSignalDisplayMode.Flashing)
                TurnSignalBlinking();
            // change gear label
            UpdateGearLabel();

            // interior lights
            foreach (KeyCode k in interiorLightKeys)
            {
                if (Input.GetKeyDown(k))
                {
                    ToggleInteriorLight();
                    break;
                }
            }
            // windshield wipers
            foreach (KeyCode k in windshieldWiperKeys)
            {
                if (Input.GetKeyDown(k))
                {
                    if (windshieldWiperAnimation != null)
                    {
                        Debug.Log("Windshield wipers");
                        windshieldWiperAnimation.Play("windshield_wipers");
                    }
                }            
            }
            // playlist control
            foreach (KeyCode k in playPauseKeys)
            {
                if (Input.GetKeyDown(k))
                {
                    carMusicPlaylist.playing = !carMusicPlaylist.playing;
                    if (carMusicPlaylist.playing)
                        carMusicPlaylist.Play();
                    else
                        carMusicPlaylist.Pause();
                }
            }
            foreach (KeyCode k in nextSongKeys)
            {
                if (Input.GetKeyDown(k))
                {
                    carMusicPlaylist.NextSong();
                }
            }
            foreach (KeyCode k in previousSongKeys)
            {
                if (Input.GetKeyDown(k))
                {
                    carMusicPlaylist.PreviousSong();
                }
            }
        }
        private void SwitchTurnSignal()
        {
            if (currentTurn != TurnState.none)
                turnDirectionSwitched = !turnDirectionSwitched;
            if (turnDirectionSwitched)
                currentTurn--;
            else
                currentTurn++;
            timeElapsed = 0;
            turnChanged = true;
        }
        private void TurnSignalBlinking()
        {
            // one-time execution upon the frame of turn signal change
            if (turnChanged)
            {
                if (currentTurn == TurnState.left)
                {
                    // visuals
                    rightSignal.inCarIndicator.enabled = false;
                    foreach (GameObject g in rightSignal.lights)
                    {
                        g.SetActive(false);
                    }
                    // sound
                    turnSignalSound.Play();
                } else if (currentTurn == TurnState.right)
                {
                    // visuals
                    leftSignal.inCarIndicator.enabled = false;
                    foreach (GameObject g in leftSignal.lights)
                    {
                        g.SetActive(false);
                    }
                    // sound
                    turnSignalSound.Play();
                } else {
                    // visuals
                    leftSignal.inCarIndicator.enabled = false;
                    rightSignal.inCarIndicator.enabled = false;
                    foreach (GameObject g in rightSignal.lights)
                    {
                        g.SetActive(false);
                    }
                    foreach (GameObject g in leftSignal.lights)
                    {
                        g.SetActive(false);
                    }
                    // sound
                    turnSignalSound.Stop();
                }
                turnChanged = false;
            }
            if (currentTurn != TurnState.none)
            {
                timeElapsed += 1;
                if (timeElapsed % blinkDuration == 0)
                {
                    if (currentTurn == TurnState.left)
                    {
                        leftSignal.inCarIndicator.enabled = !leftSignal.inCarIndicator.enabled;
                        foreach (GameObject g in leftSignal.lights) g.SetActive(!g.activeSelf);
                    } else {
                        rightSignal.inCarIndicator.enabled = !rightSignal.inCarIndicator.enabled;
                        foreach (GameObject g in rightSignal.lights) g.SetActive(!g.activeSelf);
                    }
                }
            }
        }
        private void UpdateGearLabel()
        {
            int[] vehicleData = vehicle.data.Get(Channel.Vehicle);
            if (gearLabel != null)
			{
				int gearId = vehicleData[VehicleData.GearboxGear];
				int gearMode = vehicleData[VehicleData.GearboxMode];
				bool switchingGear = vehicleData[VehicleData.GearboxShifting] != 0;
                
				switch (gearMode)
				{
					case 0:		// M
						if (gearId == 0)
							gearLabel.text = switchingGear? " " : "N";
						else if (gearId > 0)
							gearLabel.text = gearId.ToString();
						else
							{
							if (gearId == -1)
								gearLabel.text = "R";
							else
								gearLabel.text = "R" + (-gearId).ToString();
							}
						break;

					case 1:		// P
						gearLabel.text = "P";
						break;

					case 2:		// R
						if (gearId < -1)
							gearLabel.text = "R" + (-gearId).ToString();
						else
							gearLabel.text = "R";
						break;

					case 3:		// N
						gearLabel.text = "N";
						break;

					case 4:		// D
						if (gearId > 0)
							gearLabel.text = "D" + gearId.ToString();
						else
							gearLabel.text = "D";
						break;

					case 5:		// L
						if (gearId > 0)
							gearLabel.text = "L" + gearId.ToString();
						else
							gearLabel.text = "L";
						break;

					default:
						gearLabel.text = "-";
						break;
				}
			}
        }
        private void ToggleInteriorLight()
        {
            interiorLight.SetActive(!interiorLight.activeSelf);
        }
    }
}