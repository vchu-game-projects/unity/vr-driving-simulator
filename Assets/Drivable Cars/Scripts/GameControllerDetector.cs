using UnityEngine.InputSystem;
using UnityEngine;
using VehiclePhysics;
using System;

public class GameControllerDetector : MonoBehaviour
{
    public enum DeviceType { Keyboard, Gamepad, SteeringWheel }
    public DeviceType deviceType = DeviceType.Keyboard;
    private bool[] connectedDevices = {true, false, false}; // {Keyboard, Gamepad, SteeringWheel}

    public VPStandardInput playerVehicleInput;

    private void OnEnable()
    {
        // Subscribe to the device change event
        InputSystem.onDeviceChange += OnDeviceChange;
    }

    private void OnDisable()
    {
        // Unsubscribe to avoid memory leaks
        InputSystem.onDeviceChange -= OnDeviceChange;
    }

    private void OnDeviceChange(InputDevice device, InputDeviceChange change)
    {
        
        Debug.Log("Device: " + device.name + " " + change.ToString());
        // Check if a steering wheel is connected
        if (device is Joystick)
        {
            if (change == InputDeviceChange.Added || change == InputDeviceChange.HardReset 
                || change == InputDeviceChange.Reconnected)
            {
                connectedDevices[2] = true;
            } else if (change == InputDeviceChange.Removed || change == InputDeviceChange.Disconnected
                 || change == InputDeviceChange.Disabled)
            {
                connectedDevices[2] = false;
            }
        }
        // Check if a gamepad is connected
        if (device is Gamepad)
        {
            if (change == InputDeviceChange.Added || change == InputDeviceChange.HardReset
                || change == InputDeviceChange.Reconnected)
            {
                connectedDevices[1] = true;
            }
            else if (change == InputDeviceChange.Removed || change == InputDeviceChange.Disconnected
                 || change == InputDeviceChange.Disabled)
            {
                connectedDevices[1] = false;
            }
        }

        // determine which one is the primary input device
        if (connectedDevices[2])
        {
            deviceType = DeviceType.SteeringWheel;
            SteeringWheelEnabled();
        }
        else if (connectedDevices[1])
        {
            deviceType = DeviceType.Gamepad;
            GamepadEnabled();
        }
        else
        {
            deviceType = DeviceType.Keyboard;
            KeyboardEnabled();
        }

        Debug.Log("Current device type " + deviceType.ToString());
    }

    private void SteeringWheelEnabled() {
        // The mapping is based on the buttons of Logitech Driving Force GT
        playerVehicleInput.ignitionKey = KeyCode.Joystick1Button14; // "Enter" button with the red ring (on the right bottom side)
        playerVehicleInput.throttleAndBrakeAxis = "Throttle And Brake";
    }

    private void GamepadEnabled()
    {
        playerVehicleInput.throttleAndBrakeAxis = "Vertical";
    }

    private void KeyboardEnabled()
    {
        playerVehicleInput.ignitionKey = KeyCode.E;
        playerVehicleInput.throttleAndBrakeAxis = "Vertical";
    }
}