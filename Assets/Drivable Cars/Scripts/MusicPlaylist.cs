using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MusicPlaylist : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;
    private AudioClip[] audioClips;
    AudioClip downloadedClip;
    public string[] musicLinks;
    private int currentSongIdx = 0;

    public bool playing = false;
    void Start()
    {
        audioClips = new AudioClip[musicLinks.Length];
        if (audioSource != null && musicLinks.Length != 0)
            for (int i=0; i<musicLinks.Length; i++)
            {
                StartCoroutine(GetAudioClips(i));
                StopCoroutine(GetAudioClips(i));
            }
    }
    IEnumerator GetAudioClips(int i)
    {
        using (UnityWebRequest res = UnityWebRequestMultimedia.GetAudioClip(musicLinks[i], AudioType.MPEG))
        {
            yield return res.SendWebRequest();
            if (res.isNetworkError)
            {
                Debug.Log(res.error);
            } else {
                downloadedClip = DownloadHandlerAudioClip.GetContent(res);
                audioClips[i] = downloadedClip;
            }
        }
    }
    void Update()
    {
        if (playing && !audioSource.isPlaying)
        {
            NextSong();
        }
    }
    public void Pause() 
    {
        audioSource.Pause();
    }
    public void Play() 
    {
        audioSource.Play();
    }
    public void Stop() 
    {
        audioSource.Stop();
    }
    
    public void NextSong()
    {
        currentSongIdx = (currentSongIdx+1) % musicLinks.Length;
        audioSource.clip = audioClips[currentSongIdx];
        audioSource.Play();
    }
    public void PreviousSong()
    {
        currentSongIdx = (currentSongIdx-1) % musicLinks.Length;
        audioSource.clip = audioClips[currentSongIdx];
        audioSource.Play();
    }
}
