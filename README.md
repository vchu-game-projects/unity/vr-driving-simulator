# VR Driving Simulator
A VR Driving Simulator created in Unity3D.

# Plugins Used
- [Vehicle Physics Pro](https://vehiclephysics.com/)
- Unity [XR Interaction Toolkit](https://docs.unity3d.com/Packages/com.unity.xr.interaction.toolkit@3.0/manual/index.html)
- Unity [Universal Render Pipeline (URP)](https://unity.com/srp/universal-render-pipeline)

# Assets Used
### Car Models
- Aston Martin Vantage: https://www.cgtrader.com/free-3d-models/car/sport-car/aston-martin-v12-vantage-s-2016-with-hq-interior-rigged
- Honda E: https://skfb.ly/onCrI